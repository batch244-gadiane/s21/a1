// console.log("Hello World!")

let studentNumberA = "2022-1923";
let studentNumberB = "2022-1924";
let studentNumberC = "2022-1925";
let studentNumberD = "2022-1926";

// Array is simply a list of data
// [] -  Array Literal
let studentNumbers = ["2022-1923", "2022-1924", "2022-1925", "2022-1926"];
console.log(studentNumbers);
console.log(typeof studentNumbers);

// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);
console.log(typeof grades);

let computerBrands = ["Acer", "Asus", "Lenovo", "Apple", "Dell", "Samsung", "Toshiba"]
console.log(computerBrands);

// Possible but not recommended
let persons = ["Jane", "Smith", 12, true, null, undefined, {}];
console.log(persons);

// Alternative way of declaring an array
let myTasks = [
    "drink html",
    "eat javascript",
    "inhale css",
    "bake bootstrap"
]
console.log(myTasks);

// Creating an array with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// [SECTION] .length property allows us to get and set the total number of items in an array

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);// 0

// .length on strings also counts the spaces
let fullname = "Daisy Dela Cruz"
console.log(fullname.length);

// length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten by simply updating the length property
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// We can also delete the last item using decrementation
cities.length--;
console.log(cities);

// We can't do the same with strings
fullname.length = fullname.length-1;
console.log(fullname.length);
console.log(fullname);

fullname.length--;
console.log(fullname);

// We can also lengthen an array by setting the length property
let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++
theBeatles[4] = "Daisy"
console.log(theBeatles);


// [SECTION] Reading from Arrays/Accessing Elements in an Array

console.log(grades[0]);//98.5
console.log(computerBrands[5]);//Samsung
console.log(theBeatles[3]);

// Accessing an array element that does not exist will return undefined
console.log(grades[10]);

let lakerLegends = ["Kobe", "Kareem", "Magic", "Shaq", "LeBron"];
console.log(lakerLegends[3]);//Shaq
console.log(lakerLegends[4]);

// You can also store an array item in another variable
let currentLaker = lakerLegends[0];
console.log(currentLaker);

// Reassigning an array element using its index
console.log("Array before reassignment:");
console.log(lakerLegends);
lakerLegends[2] = "Pau Gasol"
console.log("Array after reassignment: ");
console.log(lakerLegends);

// Accessing the last element in an array
//Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element.

let bullsLegends = ["Jordan", "Rose", "Pippen", "Rodman", "Kukoc"];
                        //4
let lastElementIndex = bullsLegends.length - 1;

                            //4
console.log(bullsLegends[lastElementIndex]);

// You can also add it directly
                            //4
console.log(bullsLegends[bullsLegends.length - 1]);

// Adding Items into an Array

let newArr = [];
console.log(newArr[0]);//returns undefined

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

// You can alaso add items at the end of an array.
newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

// Looping over an Array
// We can use a for loop to iterate over all items in an array

                            //3
for (let index = 0; index < newArr.length; index++) {
    console.log(newArr[index]);
}

// Checks if the following items in the array are divisible by 5 or not

let numArr = [5, 12, 30, 46, 40];

for (let index = 0; index < numArr.length; index++){

    if(numArr[index] % 5 === 0) {
        console.log(numArr[index] + " is divisible by 5");
    }
    else {
        console.log(numArr[index] + " is not divisible by 5");
    }
}

// [SECTION] Multidimensional Arrays

let chessBoard = [
    ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
    ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
    ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
    ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
    ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
    ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
    ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
    ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
];
console.table(chessBoard);

console.log(chessBoard[1][4]);
console.log(chessBoard[6][7]);

console.log("Pawn moves to: " + chessBoard[1][5]);